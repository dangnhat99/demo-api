package com.example.dogapp.UI.dog

import DogEvent
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.dogapp.R
import com.example.dogapp.data.Model.DogModel
import com.example.dogapp.databinding.DogItemBinding
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dog_item.view.*
import org.greenrobot.eventbus.EventBus

class DogAdapter(
    var list: MutableList<DogModel>,
    val context: Context?,
    val detailIntent: ((Int)-> Unit)
): RecyclerView.Adapter<DogAdapter.DogViewHolder>() {

    inner class DogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = DogItemBinding.bind(itemView)

        fun bindView() {
            val item = list[adapterPosition]
            Picasso.get().load(item.url).fit().into(binding.imgAvatar)
            binding.txtName.text = item.name
            binding.txtDescribtion.text = item.bred_for
        }

        init {
            binding.btnLove.setOnClickListener {
                if (binding.btnLove.tag == "true") {
                    binding.btnLove.tag = "false"
                    binding.btnLove.setImageResource(R.drawable.ic_baseline_favorite_unselecte)

                } else {
                    binding.btnLove.tag = "true"
                    binding.btnLove.setImageResource(R.drawable.ic_baseline_favorite_selected)
                }
            }

            binding.root.setOnClickListener {
                EventBus.getDefault().postSticky(DogEvent(list[adapterPosition]))
                detailIntent(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dog_item,parent,false)
        return DogViewHolder(view)
    }

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {

        holder.bindView()
    }

    override fun getItemCount(): Int {
        return list.size
    }
}