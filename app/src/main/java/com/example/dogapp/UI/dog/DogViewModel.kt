package com.example.dogapp.UI.dog

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.dogapp.data.Model.DogModel
import com.example.tutorfinder.data.network.ApiService
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

class DogViewModel(application: Application): AndroidViewModel(application) {
    companion object {
        private val apiService by lazy { ApiService.create() }
        private var disposable: Disposable? = null
    }

    private var _dogResponse = MutableLiveData<MutableList<DogModel>>()
    var dogResponse: LiveData<MutableList<DogModel>> = _dogResponse

    private var _error = MutableLiveData<String>()
    var error: LiveData<String> = _error

    fun loadDogs() {
        disposable = apiService.getDogInfo()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    _dogResponse.value = it},
                {
                    _error.value = it.message}
            )
    }

}