package com.example.dogapp.UI.main_act

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dogapp.R
import com.example.tutorfinder.base.BaseActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun initView() {
//        TODO("Not yet implemented")
    }

    override fun initListener() {
//        TODO("Not yet implemented")
    }
}