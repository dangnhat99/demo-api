package com.example.dogapp.UI.dog

import DogEvent
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.dogapp.R
import com.example.dogapp.data.Model.DogModel
import com.example.dogapp.databinding.DogFragmentBinding
import com.example.tutorfinder.base.BaseFragment
import com.example.dogapp.base.viewBinding
import com.google.android.material.snackbar.Snackbar
import org.greenrobot.eventbus.EventBus

class DogFragment: BaseFragment(R.layout.dog_fragment) {
    private val binding by viewBinding (DogFragmentBinding:: bind)
    private lateinit var viewModel: DogViewModel
    private lateinit var loadingView: AlertDialog

    private lateinit var dogList: MutableList<DogModel>
    private lateinit var dogAdapter:DogAdapter
    override fun initView() {
        viewModel = ViewModelProvider(this).get(DogViewModel::class.java)

        val builder = context?.let { AlertDialog.Builder(it) }?.apply {
            setCancelable(false)
            setView(R.layout.loading_dialog)
        }

        if (builder != null) {
            loadingView = builder.create()
            loadingView.show()
        }

        dogList = arrayListOf()
        dogAdapter = DogAdapter(dogList, context) { startDetailFragment() }
        binding.recyclerView.apply {
            layoutManager = GridLayoutManager(context,2)
            adapter = dogAdapter
        }

        viewModel.loadDogs()
    }

    private fun startDetailFragment() {
        navController.navigate(R.id.DogFragmentToDetailFragment)
    }

    override fun initListener() {
        viewModel.dogResponse.observe(this, {
            dogList.clear()
            dogList.addAll(it)
            Log.d("DogFRAGMENT", "dog count: "+ it.size)
            dogAdapter.notifyDataSetChanged()
            loadingView.dismiss()
        })

        viewModel.error.observe(this, {
            Snackbar.make(binding.root, "error:$it", Snackbar.LENGTH_SHORT).show()
        })
    }
}