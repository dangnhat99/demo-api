package com.example.dogapp.UI.detail

import DogEvent
import com.example.dogapp.R
import com.example.tutorfinder.base.BaseFragment
import com.example.dogapp.base.viewBinding
import com.example.dogapp.data.Model.DogModel
import com.example.dogapp.databinding.DetailFragmentBinding
import com.squareup.picasso.Picasso
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class DetailFragment : BaseFragment(R.layout.detail_fragment) {

    val binding by viewBinding (DetailFragmentBinding:: bind)
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this);
    }
    override fun initView() {
//        TODO("Not yet implemented")
    }

    override fun initListener() {
//        TODO("Not yet implemented")
    }

    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: DogEvent) { /* Do something */
        updateUI(event.item)
    }

    fun updateUI(dog:DogModel) {
        binding.breedFor.text = dog.bred_for
        binding.breedGroup.text = dog.breed_group
        binding.height.text = dog.height.metric + " cm"
        binding.weight.text = dog.weight.metric + " kg"
        Picasso.get().load(dog.url).fit().into(binding.image)
        binding.lifeSpan.text = dog.life_span
        binding.name.text= dog.name
        binding.origin.text = dog.origin
        binding.temperament.text = dog.temperament


    }

}