package com.example.tutorfinder.data.network
import com.example.dogapp.data.Model.DogModel
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import io.reactivex.rxjava3.core.Observable
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.*

const val BASE_URL = "https://raw.githubusercontent.com/DevTides/DogsApi/"

interface ApiService {

    @Headers("Content-Type: application/json")
    @GET("master/dogs.json?fbclid=IwAR1JkxssVBKMghLS9Oo3ujrXmbdN8CMNiXowsuTNbKbq63nUp7kQiYwOBjE/")
    fun getDogInfo(
    ): Observable<MutableList<DogModel>>

    companion object {
        private val logging: HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        private val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(logging).build()

        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(client)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}